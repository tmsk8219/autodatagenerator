//
//  Student.swift
//  AutoDataGenerator
//
//  Created by Hikaru KISHIMOTO on 2016/03/24.
//  Copyright © 2016年 Hikaru KISHIMOTO. All rights reserved.
//

import Cocoa

class Student: NSObject {
    var name: String?
    var course: String?
    var stage: String?
    var time: Int?
    
    init(_name: String, _course: String, _stage: String, _time: Int) {
        self.name = _name
        self.course = _course
        self.stage = _stage
        self.time = _time
    }
}


