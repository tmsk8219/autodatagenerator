//
//  ViewController.swift
//  AutoDataGenerator
//
//  Created by Hikaru KISHIMOTO on 2016/03/24.
//  Copyright © 2016年 Hikaru KISHIMOTO. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {
    
    
    @IBOutlet var inNames: NSMatrix!
    @IBOutlet var inCourses: NSMatrix!
    @IBOutlet var inStages: NSMatrix!
    @IBOutlet var inTimes: NSMatrix!
    
    
    
    var students: [[String]] = [[]]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        // 入力初期化
        let courses = ["iPhone", "Web", "Scratch", "GameSalad", "Minecraft"]
        let stages = ["Junior", "Basic", "Professional"]
        var times: [String] = []
        for(var i = 1; i <= 48; i++){
            times.append(String(i))
        }
        
        for(var i = 0; i < 8; i++){
            (inCourses.cellWithTag(i)! as! NSComboBoxCell).addItemsWithObjectValues(courses)
            (inStages.cellWithTag(i)! as! NSComboBoxCell).addItemsWithObjectValues(stages)
            (inTimes.cellWithTag(i)! as! NSComboBoxCell).addItemsWithObjectValues(times)
            
        }
        
        
    }
    
    override var representedObject: AnyObject? {
        didSet {
            // Update the view, if already loaded.
        }
    }
    
    func tapGenerate(){
    }
    
    @IBAction func onGenerate(sender: NSButton) {
        students.removeAll()
        
        for (var i = 0; i < 8; i++){
            var student: [String] = []
            let name = inNames.cellWithTag(i)! as! NSTextFieldCell
            let course = inCourses.cellWithTag(i)! as! NSComboBoxCell
            let stage = inStages.cellWithTag(i)! as! NSComboBoxCell
            let time = inTimes.cellWithTag(i)! as! NSComboBoxCell
            if((name.stringValue != "") && (course.stringValue != "") && (stage.stringValue != "") && (time.stringValue != "")){
                student.append(name.stringValue)
                student.append(course.stringValue)
                student.append(stage.stringValue)
                student.append(time.stringValue)
                students.append(student)
            }
        }
    }
    
    
    
    @IBAction func onCheck(sender: NSButton) {
        dataPaser()
    }
    
    
    func dataPaser(){
        for(var i = 0; i < students.count; i++){
            // コース名を変換
            if(students[i][1] == "iPhone"){
                students[i][1] = "i"
            }else if(students[i][1] == "Web"){
                students[i][1] = "w"
            }else if(students[i][1] == "Scratch"){
                students[i][1] = "s"
            }else if(students[i][1] == "GameSalad"){
                students[i][1] = "g"
            }else if(students[i][1] == "Minecraft"){
                students[i][1] = "m"
            }
            
            // ステージ名を変換
            if(students[i][2] == "Junior"){
                students[i][2] = "j"
            }else if(students[i][2] == "Basic"){
                students[i][2] = "b"
            }else if(students[i][2] == "Professional"){
                students[i][2] = "p"
            }
        }
    }
    
    @IBAction func onDisplay(sender: NSButton){
        for student in students{
            for data in student{
                NSLog(data)
            }
        }
    }

}


















//    @IBOutlet weak var tableView: NSTableView!

//    func numberOfRowsInTableView(aTableView: NSTableView) -> Int{
//        return students.count
//    }
//
//    func tableView(aTableView: NSTableView, objectValueForTableColumn aTableColumn: NSTableColumn?, row rowIndex: Int) -> AnyObject?{
//        let name = students[rowIndex][0]
//        let course = students[rowIndex][1]
//        let stage = students[rowIndex][2]
//        let time = students[rowIndex][3]
//
//
//
//        let columnName = aTableColumn?.identifier
//
//        if columnName == "Name"{
//            return name
//        }else if columnName == "Course"{
//            return course
//        }else if columnName == "Stage"{
//            return stage
//        }else if columnName == "Time"{
//            return time
//        }
//        return ""
//    }
//    @IBAction func tapGenerate(sender: NSButton) {
//        tableView.reloadData()
//    }
//
//    @IBAction func tapAdd(sender: NSButton) {
//        // 追加する各入力から取得
//        let addName: String = inName.stringValue
//        let addCourse: String = inCourse.stringValue
//        let addStage: String = inStage.stringValue
//        let addTime: String = inTime.stringValue
//
//        let addStudent: [String] = [addName, addCourse, addStage, addTime]
//        for str in addStudent{
//            NSLog(str)
//        }
//        students.append(addStudent)
//        NSLog(String(students.count))
//
//        // TableViewに追加する
//        tableView.reloadData()
//    }