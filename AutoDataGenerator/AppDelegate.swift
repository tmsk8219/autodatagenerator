//
//  AppDelegate.swift
//  AutoDataGenerator
//
//  Created by Hikaru KISHIMOTO on 2016/03/24.
//  Copyright © 2016年 Hikaru KISHIMOTO. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

